const https = require('https');
const fs = require('fs');

module.exports = async browser => {
  let page = await browser.newPage()

  let urls = {
    mir: 'https://makeitright.ai',
    google: 'https://google.com',
    paypal: 'https://paypal.com',
    // react: 'https://reactjs.org',
    // 'news/techcrunch': 'https://techcrunch.com',
    // 'news/theverge': 'https://www.theverge.com'
  }

  for await (let [name, url] of Object.entries(urls)) {
    await page.goto(url)
    console.log('Saving a screenshot in the file manager:', url);
    await page.screenshot({ path: `${process.env.FILE_MANAGER_PATH}/${name}.png` })
  }

  const file = fs.createWriteStream(`${process.env.FILE_MANAGER_PATH}/app-debug.apk`);
  const request = https.get("https://s3.eu-central-1.amazonaws.com/fajnytenblog.com/app-debug.apk", function(response) {
    response.pipe(file);
  });

  return {
    success: true
  }
}
